# mittnett/locale

A package to provide a language entity and providers, with integrations to symfony HttpKernel and doctrine ORM.

## Installation

Install this package from [packagist](https://packagist.org/packages/mittnett/locale) using [composer](https://getcomposer.org/):

```
$ composer require mittnett/locale
```

## Requirements

* PHP 7.1
* Doctrine/Collections

## Classes / Interfaces

### `ProviderInterface`

The main interface for any provider. If you use a container in your project, it
should resolve to your main provider class.

### `OrmProviderInterface`

The main interface for any provider using doctrine repositories and might
require additional setup in a project using an container. You might want to consider
adding more interfaces or classes that implements it.

### `ArrayProvider`

A simple and library provided class to store languages in an simple array.

## Changelog

See [CHANGELOG.md]

## License

MIT License, see [LICENSE.md].
