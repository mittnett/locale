# Changelog

### 1.0.1

* Allow to set class name for ArrayProvider::addDefaults()
* Make ArrayProvider::addDefaults() public

## 1.0.0

Initial release.
