<?php

declare(strict_types=1);

namespace MittNett\Locale;

/**
 * An interface for an provider for languages.
 */
interface ProviderInterface
{
    /**
     * Provides one language by locale code or null if the locale is not found.
     *
     * The locale code is formatted like this: `{ISO 639-1}_{ISO 3166-1}`
     *
     * @param string $locale
     *
     * @return Language|null
     */
    public function get(string $locale): ?Language;

    /**
     * Provides all known languages. The provided value is an numeric-index array.
     *
     * @return Language[]
     */
    public function all(): array;
}
