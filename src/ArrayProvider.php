<?php

declare(strict_types=1);

namespace MittNett\Locale;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

class ArrayProvider implements ProviderInterface
{
    /**
     * @var Collection|Language[]
     */
    private $languages;

    public function __construct()
    {
        $this->languages = new ArrayCollection();
    }

    /**
     * @param string $className
     */
    public function addDefaults($className = Language::class)
    {
        $this->languages->set('en_GB', new $className('en_GB', 'English (United Kingdom)'));
        $this->languages->set('nb_NO', new $className('nb_NO', 'Norwegian Bokmål'));
    }

    /**
     * @inheritDoc
     */
    public function get(string $locale): ?Language
    {
        return $this->languages->get($locale);
    }

    /**
     * @inheritDoc
     */
    public function all(): array
    {
        return $this->languages->getValues();
    }
}
