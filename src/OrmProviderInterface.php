<?php

declare(strict_types=1);

namespace MittNett\Locale;

/**
 * Interface to describe an provider using an ORM related method
 * to get languages.
 */
interface OrmProviderInterface extends ProviderInterface
{
}
